use clap::Parser;
use regex::Regex;

#[derive(Parser)]
pub(crate) struct Command {
    /// Run second puzzle
    #[arg(short = '2', long)]
    part2: bool,
}

impl Command {
    pub(crate) fn execute(&self, input: &str) -> u32 {
        match self.part2 {
            false => first(&input),
            true => second(&input),
        }
    }
}

fn first(input: &str) -> u32 {
    let re = Regex::new(r"^\D*(?P<first>\d)(?:.*(?P<last>\d))?\D*$").unwrap();
    input.lines().fold(0, |acc: u32, line| {
        let caps = re.captures(line).unwrap();
        if let Some(last) = caps.name("last") {
            acc + format!("{}{}", &caps["first"], last.as_str())
                .parse::<u32>()
                .unwrap()
        } else {
            acc + format!("{}{}", &caps["first"], &caps["first"])
                .parse::<u32>()
                .unwrap()
        }
    })
}

fn match_num_word(word: &str) -> &str {
    match word {
        "one" => "1",
        "two" => "2",
        "three" => "3",
        "four" => "4",
        "five" => "5",
        "six" => "6",
        "seven" => "7",
        "eight" => "8",
        "nine" => "9",
        _ => word,
    }
}

fn second(input: &str) -> u32 {
    let re = Regex::new(r"^.*?(?P<first>\d|one|two|three|four|five|six|seven|eight|nine)(?:.*(?P<last>\d|one|two|three|four|five|six|seven|eight|nine))?.*$").unwrap();
    input.lines().fold(0, |acc: u32, line| {
        let caps = re.captures(line).unwrap();
        if let Some(last) = caps.name("last") {
            acc + format!(
                "{}{}",
                match_num_word(&caps["first"]),
                match_num_word(last.as_str())
            )
            .parse::<u32>()
            .unwrap()
        } else {
            acc + format!(
                "{}{}",
                match_num_word(&caps["first"]),
                match_num_word(&caps["first"])
            )
            .parse::<u32>()
            .unwrap()
        }
    })
}

#[cfg(test)]
mod tests {
    use super::{first, second};

    #[test]
    fn test_first() {
        let result = first(
            "1abc2\n\
            pqr3stu8vwx\n\
            a1b2c3d4e5f\n\
            treb7uchet",
        );
        assert_eq!(result, 142)
    }

    #[test]
    fn test_second() {
        let result = second(
            "two1nine\n\
            eightwothree\n\
            abcone2threexyz\n\
            xtwone3four\n\
            4nineeightseven2\n\
            zoneight234\n\
            7pqrstsixteen",
        );
        assert_eq!(result, 281)
    }
}
