use clap::Parser;
use std::cmp;

#[derive(Parser)]
pub(crate) struct Command {
    /// Run second puzzle
    #[arg(short = '2', long)]
    part2: bool,
}

impl Command {
    pub(crate) fn execute(&self, input: &str) -> u32 {
        match self.part2 {
            false => first(&input),
            true => second(&input),
        }
    }
}

struct Game {
    id: u8,
    sets: Vec<CubeSet>,
}

struct CubeSet {
    red: u8,
    green: u8,
    blue: u8,
}

fn parse_set(line: &str) -> CubeSet {
    let mut result = CubeSet {
        red: 0,
        green: 0,
        blue: 0,
    };
    for colour in line.split(", ") {
        let mut colour_iter = colour.split(" ");
        let num = colour_iter.next().unwrap().parse::<u8>().unwrap();
        match colour_iter.next().unwrap() {
            "red" => result.red = num,
            "green" => result.green = num,
            "blue" => result.blue = num,
            _ => {}
        }
    }
    result
}

fn parse_game(line: &str) -> Game {
    let mut line_iter = line.split(": ");
    let game = line_iter
        .next()
        .unwrap()
        .split(" ")
        .skip(1)
        .next()
        .unwrap()
        .parse::<u8>()
        .unwrap();
    let sets: Vec<CubeSet> = line_iter
        .next()
        .unwrap()
        .split("; ")
        .map(parse_set)
        .collect();
    Game {
        id: game,
        sets: sets,
    }
}

fn set_is_possible(set: &CubeSet, reference: &CubeSet) -> bool {
    set.red <= reference.red && set.green <= reference.green && set.blue <= reference.blue
}

fn game_is_possible(game: &Game, reference: &CubeSet) -> bool {
    game.sets.iter().all(|set| set_is_possible(set, reference))
}

fn first(input: &str) -> u32 {
    let reference_set = CubeSet {
        red: 12,
        green: 13,
        blue: 14,
    };
    input
        .lines()
        .map(parse_game)
        .filter(|game| game_is_possible(game, &reference_set))
        .fold(0, |acc, game| acc + game.id as u32)
}

fn find_min_set(game: &Game) -> CubeSet {
    let mut result = CubeSet {
        red: 0,
        green: 0,
        blue: 0,
    };
    game.sets.iter().for_each(|s| {
        result.red = cmp::max(s.red, result.red);
        result.green = cmp::max(s.green, result.green);
        result.blue = cmp::max(s.blue, result.blue);
    });
    result
}

fn set_power(set: &CubeSet) -> u32 {
    set.red as u32 * set.green as u32 * set.blue as u32
}

fn second(input: &str) -> u32 {
    input
        .lines()
        .map(|line| set_power(&find_min_set(&parse_game(line))))
        .sum::<u32>()
}

mod tests {
    use super::{first, second};

    #[test]
    fn test_first() {
        let result = first(
            "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n\
            Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n\
            Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n\
            Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n\
            Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
        );
        assert_eq!(result, 8)
    }

    #[test]
    fn test_second() {
        let result = second(
            "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n\
            Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n\
            Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n\
            Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n\
            Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
        );
        assert_eq!(result, 2286)
    }
}
