use clap::Parser;
use std::collections::{HashMap, HashSet, VecDeque};

#[derive(Parser)]
pub(crate) struct Command {
    /// Run second puzzle
    #[arg(short = '2', long)]
    part2: bool,
}

impl Command {
    pub(crate) fn execute(&self, input: &str) -> u32 {
        match self.part2 {
            false => first(&input),
            true => second(&input),
        }
    }
}

fn parse_input(input: &str) -> (HashMap<(u8, u8), char>, HashMap<(u8, u8), char>) {
    let mut numbers = HashMap::<(u8, u8), char>::new();
    let mut parts = HashMap::<(u8, u8), char>::new();
    for (i, line) in input.lines().enumerate() {
        for (j, c) in line.char_indices() {
            if c.is_numeric() {
                numbers.insert((i as u8, j as u8), c);
            } else if c != '.' {
                parts.insert((i as u8, j as u8), c);
            }
        }
    }
    (numbers, parts)
}

fn extract_part_number(numbers: &HashMap<(u8, u8), char>, pos: &(u8, u8)) -> u32 {
    let mut chars = VecDeque::<&char>::new();
    let mut look_behind = (pos.1 - 2..=pos.1).rev();
    while let Some(p) = look_behind.next() {
        if let Some(c) = numbers.get(&(pos.0, p)) {
            chars.push_front(c);
        } else {
            break;
        }
    }
    let mut look_ahead = pos.1 + 1..=pos.1 + 2;
    while let Some(p) = look_ahead.next() {
        if let Some(c) = numbers.get(&(pos.0, p)) {
            chars.push_back(c);
        } else {
            break;
        }
    }
    chars.into_iter().collect::<String>().parse().unwrap()
}

fn find_part_numbers(numbers: &HashMap<(u8, u8), char>, pos: &(u8, u8)) -> HashSet<u32> {
    let mut hits = HashSet::<u32>::new();
    for i in pos.0 - 1..=pos.0 + 1 {
        for j in pos.1 - 1..=pos.1 + 1 {
            if numbers.contains_key(&(i, j)) {
                hits.insert(extract_part_number(&numbers, &(i, j)));
            }
        }
    }
    hits
}

fn first(input: &str) -> u32 {
    let (numbers, parts) = parse_input(input);
    parts.keys().fold(0, |acc, p| {
        acc + find_part_numbers(&numbers, p).iter().sum::<u32>()
    })
}

fn gear_ratio(numbers: &HashMap<(u8, u8), char>, pos: &(u8, u8)) -> u32 {
    let parts = find_part_numbers(numbers, pos);
    if parts.len() != 2 {
        return 0;
    }
    parts.iter().product()
}

fn second(input: &str) -> u32 {
    let (numbers, parts) = parse_input(input);
    parts
        .iter()
        .filter(|(_, c)| **c == '*')
        .fold(0, |acc, (p, _)| acc + gear_ratio(&numbers, p))
}

mod tests {
    use super::{first, second};

    #[test]
    fn test_first() {
        let result = first(
            "467..114..\n\
            ...*......\n\
            ..35..633.\n\
            ......#...\n\
            617*......\n\
            .....+.58.\n\
            ..592.....\n\
            ......755.\n\
            ...$.*....\n\
            .664.598..",
        );
        assert_eq!(result, 4361);
    }

    #[test]
    fn test_second() {
        let result = second(
            "467..114..\n\
            ...*......\n\
            ..35..633.\n\
            ......#...\n\
            617*......\n\
            .....+.58.\n\
            ..592.....\n\
            ......755.\n\
            ...$.*....\n\
            .664.598..",
        );
        assert_eq!(result, 467835);
    }
}
