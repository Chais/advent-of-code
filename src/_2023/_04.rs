use clap::Parser;
use std::cmp;
use std::collections::HashSet;

#[derive(Parser)]
pub(crate) struct Command {
    /// Run second puzzle
    #[arg(short = '2', long)]
    part2: bool,
}

impl Command {
    pub(crate) fn execute(&self, input: &str) -> u32 {
        match self.part2 {
            false => first(&input),
            true => second(&input),
        }
    }
}

struct Card {
    win: HashSet<u8>,
    have: HashSet<u8>,
}

impl Card {
    fn points(&self) -> u16 {
        let len = self.have.intersection(&self.win).count();
        match len {
            0 | 1 => len as u16,
            _ => 2_u16.pow(len as u32 - 1),
        }
    }
    fn matches(&self) -> usize {
        self.have.intersection(&self.win).count()
    }
}

fn parse_card(line: &str) -> Card {
    let mut num_iter = line.split(':').skip(1).next().unwrap().split('|');
    let win: HashSet<u8> = num_iter
        .next()
        .unwrap()
        .split(' ')
        .filter_map(|s| s.parse().ok())
        .collect();
    let have: HashSet<u8> = num_iter
        .next()
        .unwrap()
        .split(' ')
        .filter_map(|s| s.parse().ok())
        .collect();
    Card { win, have }
}

fn parse_input(input: &str) -> Vec<Card> {
    input.lines().map(|l| parse_card(l)).collect::<Vec<Card>>()
}

fn first(input: &str) -> u32 {
    let cards = parse_input(input);
    cards.iter().map(|c| c.points() as u32).sum::<u32>()
}

fn second(input: &str) -> u32 {
    let cards = parse_input(input);
    let c_len = cards.len();
    let mut copies = vec![1_u32; c_len];
    cards
        .iter()
        .map(|c| c.matches())
        .enumerate()
        .for_each(|(i, p)| {
            let c_i = copies[i];
            for c in &mut copies[i + 1..cmp::min(i + 1 + p, c_len)] {
                *c += c_i;
            }
        });
    copies.iter().sum::<u32>()
}

mod tests {
    use super::{first, second};

    #[test]
    fn test_first() {
        let result = first(
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\n\
            Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\n\
            Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\n\
            Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\n\
            Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\n\
            Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
        );
        assert_eq!(result, 13)
    }

    #[test]
    fn test_second() {
        let result = second(
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\n\
            Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\n\
            Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\n\
            Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\n\
            Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\n\
            Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
        );
        assert_eq!(result, 30)
    }
}
