use clap::Parser;
use std::collections::HashMap;
use std::hash::Hash;
use std::ops::{Add, Range, Sub};

#[derive(Parser)]
pub(crate) struct Command {
    /// Run second puzzle
    #[arg(short = '2', long)]
    part2: bool,
}

impl Command {
    pub(crate) fn execute(&self, input: &str) -> u64 {
        match self.part2 {
            false => first(&input),
            true => second(&input),
        }
    }
}

struct RangeMap<T> {
    maps: HashMap<Range<T>, Range<T>>,
}

impl<T: Add<Output = T> + Copy + Eq + Hash + PartialOrd + Sub<Output = T>> RangeMap<T> {
    fn new() -> RangeMap<T> {
        RangeMap {
            maps: HashMap::new(),
        }
    }

    fn insert(&mut self, k: Range<T>, v: Range<T>) {
        self.maps.insert(k, v);
    }

    fn get(&self, k: T) -> T {
        for m in self.maps.iter() {
            if m.0.contains(&k) {
                return m.1.start + k - m.0.start;
            }
        }
        k
    }
}

fn parse_block(input: &str) -> (String, (String, RangeMap<u64>)) {
    let mut line_iter = input.lines();
    let name = line_iter.next().unwrap().split(' ').next().unwrap();
    let mut name_iter = name.split("-to-");
    let mut map = RangeMap::<u64>::new();
    for range_map in line_iter {
        let tokens: Vec<u64> = range_map
            .split(' ')
            .filter_map(|n| n.parse().ok())
            .collect();
        map.insert(
            tokens[1]..tokens[1] + tokens[2] + 1,
            tokens[0]..tokens[0] + tokens[2] + 1,
        );
    }
    (
        name_iter.next().unwrap().to_string(),
        (name_iter.next().unwrap().to_string(), map),
    )
}

fn parse_input(input: &str) -> (Vec<u64>, HashMap<String, (String, RangeMap<u64>)>) {
    let mut block_iter = input.split("\n\n");
    let seeds: Vec<u64> = block_iter
        .next()
        .unwrap()
        .split(':')
        .skip(1)
        .next()
        .unwrap()
        .split(' ')
        .filter_map(|s| s.parse().ok())
        .collect();
    let maps = block_iter.map(parse_block).collect();
    (seeds, maps)
}

fn first(input: &str) -> u64 {
    let (seeds, maps) = parse_input(input);
    // let locations: Vec<u64> = seeds
    //     .into_iter()
    //     .map(|s| maps.iter().fold(s, |acc, m| m.1.get(acc)))
    //     .collect();
    let locations: Vec<u64> = seeds
        .into_iter()
        .map(|s| {
            let mut step = s;
            let mut from = &"seed".to_string();
            while !from.contains("location") {
                let (to, m) = maps.get(from).unwrap();
                step = m.get(step);
                from = to;
            }
            step
        })
        .collect();
    locations.into_iter().min().unwrap()
}

fn second(input: &str) -> u64 {
    0
}

mod tests {
    use super::{first, second};

    #[test]
    fn test_first() {
        let result = first(
            "seeds: 79 14 55 13\n\
            \n\
            seed-to-soil map:\n\
            50 98 2\n\
            52 50 48\n\
            \n\
            soil-to-fertilizer map:\n\
            0 15 37\n\
            37 52 2\n\
            39 0 15\n\
            \n\
            fertilizer-to-water map:\n\
            49 53 8\n\
            0 11 42\n\
            42 0 7\n\
            57 7 4\n\
            \n\
            water-to-light map:\n\
            88 18 7\n\
            18 25 70\n\
            \n\
            light-to-temperature map:\n\
            45 77 23\n\
            81 45 19\n\
            68 64 13\n\
            \n\
            temperature-to-humidity map:\n\
            0 69 1\n\
            1 0 69\n\
            \n\
            humidity-to-location map:\n\
            60 56 37\n\
            56 93 4",
        );
        assert_eq!(result, 35)
    }

    #[test]
    fn test_second() {
        panic!()
    }
}
