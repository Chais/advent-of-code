use clap::Parser;

mod _01;
mod _02;
mod _03;
mod _04;
mod _05;

#[derive(Parser)]
pub(crate) struct Command {
    #[clap(subcommand)]
    subcommand: DayCommand,
}

impl Command {
    pub(crate) fn execute(&self, input: &str) -> u64 {
        self.subcommand.execute(input)
    }
}

#[derive(Parser)]
enum DayCommand {
    /// Run day 01 puzzle
    _01(_01::Command),
    /// Run day 02 puzzle
    _02(_02::Command),
    /// Run day 03 puzzle
    _03(_03::Command),
    /// Run day 04 puzzle
    _04(_04::Command),
    /// Run day 04 5uzzle
    _05(_05::Command),
}

impl DayCommand {
    fn execute(&self, input: &str) -> u64 {
        match self {
            DayCommand::_01(cmd) => cmd.execute(&input) as u64,
            DayCommand::_02(cmd) => cmd.execute(&input) as u64,
            DayCommand::_03(cmd) => cmd.execute(&input) as u64,
            DayCommand::_04(cmd) => cmd.execute(&input) as u64,
            DayCommand::_05(cmd) => cmd.execute(&input),
        }
    }
}
