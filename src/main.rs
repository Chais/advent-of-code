use clap::Parser;
use std::fs;

mod _2023;

#[derive(Parser)]
struct Cli {
    /// Path to input
    input: String,
    #[clap(subcommand)]
    subcommand: YearCommand,
}

#[derive(Parser)]
enum YearCommand {
    /// Run puzzles from 2023
    _2023(_2023::Command),
}

impl YearCommand {
    fn execute(&self, input: &str) -> u64 {
        match self {
            YearCommand::_2023(cmd) => cmd.execute(&input),
        }
    }
}

fn main() {
    let cli = Cli::parse();
    if let Ok(input) = fs::read_to_string(cli.input) {
        println!("{}", cli.subcommand.execute(&input))
    } else {
        println!("Error reading input.")
    }
}
